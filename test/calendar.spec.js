const moment = require('moment')
const chai = require('chai')

chai.use(require('chai-http'))

const server = 'http://localhost:8080'
const request = chai.request
const expect = chai.expect

describe('/api/v1/calendar', () => {
  describe('#GET', () => {
    it('should calendar with events', done => {
      request(server).get('/calendar')
        .end((err, res) => {
          expect(res.type).to.eql('application/json')
          expect(res.body.calendar.length).to.eql(42)
          done()
        })
    })
  })
})