const moment = require('moment')
const chai = require('chai')

chai.use(require('chai-http'))

const server = 'http://localhost:8080'
const request = chai.request
const expect = chai.expect

describe('/api/v1/event', () => {
  const eventsId = []

  describe('#POST', () => {
    it('should not create event', done => {
      request(server).post('/event')
        .send({
          event: {
            date: moment().unix() + 432000
          }
        })
        .end((err, res) => {
          expect(res.type).to.eql('application/json')
          expect(res.body.message).to.eql('Не верно ввели данные!')
        })

      request(server).post('/event')
        .end((err, res) => {
          expect(res.type).to.eql('application/json')
          expect(res.body.message).to.eql('Вы не выбрали дату и время!')

          done()
        })
    })
    it('should create event', done => {
      request(server).post('/event')
        .send({
          event: {
            user: 'ewfueiwhr32hf32hf9283923ue32',
            date: moment().unix() + 432000
          }
        })
        .end((err, res) => {
          expect(res.type).to.eql('application/json')
          expect(res.body.message).to.eql('Ваша запись добавлена!')
          done()
        })
    })
  })

  describe('#GET', () => {
    it('should return array of events', done => {
      request(server).get('/event')
        .end((err, res) => {
          expect(res.type).to.eql('application/json')
          expect(res.body.events).to.be.an('array')

          eventsId.push(res.body.events[0].id)
          eventsId.push(res.body.events[1].id)
          eventsId.push(res.body.events[2].id)
          eventsId.push(res.body.events[3].id)
          eventsId.push(res.body.events[4].id)
          eventsId.push(res.body.events[5].id)

          done()
        })
    })
    it('should return one event', done => {
      request(server).get(`/event/${eventsId[0]}`)
        .end((err, res) => {
          expect(res.type).to.eql('application/json')
          expect(res.body.event.id).to.eql(eventsId[0])
          done()
        })
    })
  })

  describe('#PUT', () => {
    it('should not update the event without body', done => {
      request(server).put(`/event/${eventsId[1]}`)
        .send({})
        .end((err, res) => {
          expect(res.type).to.eql('application/json')
          expect(res.body.message).to.eql('Вы ничего не изменили!')

          done()
        })
    })
    it('should update the event date', done => {
      request(server).put(`/event/${eventsId[2]}`)
        .send( { date: moment().add({days: 3}).unix() } )
        .end((err, res) => {
          expect(res.type).to.eql('application/json')
          expect(res.body.message).to.eql('Вы успешно изменили дату!')

          done()
        })
    })
    it('should to approve the event', done => {
      request(server).put(`/event/${eventsId[3]}`)
        .send({ approved: true })
        .end((err, res) => {
          expect(res.type).to.eql('application/json')
          expect(res.body.message).to.eql('Вы успешно подтвердили!')

          done()
        })
    })
    it('should update date and approve the event', done => {
      request(server).put(`/event/${eventsId[4]}`)
        .send({
          approved: true,
          date: moment().add({days: 3}).unix()
        })
        .end((err, res) => {
          expect(res.type).to.eql('application/json')
          expect(res.body.message).to.eql('Вы успешно изменили дату и подтвердили!')

          done()
        })
    })
  })

  describe('#DELETE', () => {
    it('should not update the event without body', done => {
      request(server).delete(`/event/${eventsId[5]}`)
        .end((err, res) => {
          expect(res.type).to.eql('application/json')
          expect(res.body.message).to.eql('Вы успешно удалили событие!')

          done()
        })
    })
  })
})