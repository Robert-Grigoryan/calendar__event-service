const moment = require('moment')

const Time = {
  calendar: function (from = moment().date(1)) {
    const result = [];
    const firstMonday = moment(from).format('d') === '1'
      ? from : moment(from).add({
        days: 1 - moment(from).format('d')
      })

    for (let i = 0; i < 42; i++) result.push({
      timestamp: moment(firstMonday).add({
        days: i
      }).unix()
    })

    return result
  }
}

module.exports = Time