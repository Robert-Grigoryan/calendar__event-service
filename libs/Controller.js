const Controller = function (req, res, next) {
  this.req = req
  this.res = res
  this.next = next
}

Controller.prototype.methodUndefined = function () {
  this.res.status(403)
  this.res.json({message: 'Method undefined'})

  return this.next()
}

module.exports = Controller