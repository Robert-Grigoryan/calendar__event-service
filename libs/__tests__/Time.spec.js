const chai = require('chai')
const moment = require('moment')
const Time = require('../Time')

const expect = chai.expect

describe('class Time', () => {
  it('should return dates array for the date 2018-09-01', done => {
    const calendar = Time.calendar(moment('2018-09-01'))

    const firstDay = calendar[0].timestamp
    const lastDay = calendar[calendar.length - 1].timestamp

    expect(calendar.length).to.eql(42)
    expect(moment.unix(firstDay).format('d')).to.eql('1')
    expect(moment.unix(firstDay).format('DD')).to.eql('27')
    expect(moment.unix(lastDay).format('d')).to.eql('0')
    expect(moment.unix(lastDay).format('DD')).to.eql('07')
    done()
  })
})
