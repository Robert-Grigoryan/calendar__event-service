const moment = require('moment')

const Controller = require('../../libs/Controller')
const Time = require('../../libs/Time')
const EventModel = require('../models/event')
/**
 * @param req
 * @param res
 * @param next
 * @constructor
 */
const Calendar = function (req, res, next) {
  Controller.call(this, req, res, next)
}

/**
 * @type {Controller}
 */
Calendar.prototype = Object.create(Controller.prototype)

/**
 * @returns {next}
 */
Calendar.prototype.getCalendar = async function () {
  const events = await EventModel.find().select({ __v: 0 })

  const scroll = this.req.query.scroll ? this.req.query.scroll : 0
  const month = moment().add({month: scroll}).date(1)

  const calendar = Time.calendar(month)
    .map(date => {
      const tmp = []
      events.forEach(model => {
        if (moment.unix(model.date).format('MMM-DD-Y') === moment.unix(date.timestamp).format('MMM-DD-Y')) {
          const event = { ...model._doc }
          event.id = model['_id']
          delete event['_id']
          
          tmp.push(event)
        }
      })

      if (tmp.length > 0) {
        return { ...date, events: tmp }
      } else {
        return { ...date }
      }
    })

  this.res.json({ calendar })

  return this.next()
}

module.exports = (req, res, next) => {

  const calendar = new Calendar(req, res, next)

  switch (req.method) {
    case 'GET':
      calendar.getCalendar()
      break
    default:
      calendar.methodUndefined()
  }
}
