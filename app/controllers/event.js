const Controller = require('../../libs/Controller')
const EventModel = require('../models/event')
/**
 * @param req
 * @param res
 * @param next
 * @constructor
 */
const EventController = function (req, res, next) {
  Controller.call(this, req, res, next)
}

/**
 * @type {Controller}
 */
EventController.prototype = Object.create(Controller.prototype)

/**
 * @returns {next}
 */
EventController.prototype.getEvents = async function () {
  const id = this.req.params.id

  if (id) {
    try {
      const model = await EventModel.findById(id).select({ __v: 0 })
      const event = { ...model._doc }
      event.id = model._id
      delete event._id

      this.res.json({ event })
    } catch (error) {
      this.res.json({ message: 'Такого события не существует!' })
    }
  } else {
    try {
      const model = await EventModel.find().select({ __v: 0 })
      const events = model.map(tmp => {
        const event = { ...tmp._doc }
        event.id = tmp._id
        delete event._id

        return event
      })

      this.res.json({ events })
    } catch (error) {
      this.res.status(503)
      this.res.json({ message: error.message })
    }
  }
}

EventController.prototype.postEvent = function () {
  const body = this.req.body

  if (!body) {
    this.res.status(403)
    this.res.json({ message: 'Вы не выбрали дату и время!' })
  } else {
    const event = new EventModel(body.event)
    event.validate(err => {
      if (err) {
        this.res.status(403)
        this.res.json({ message: 'Не верно ввели данные!' })
      } else {
        event.save()
        this.res.json({ message: 'Ваша запись добавлена!' })
      }
    })
  }
}

EventController.prototype.putEvent = async function () {
  const id = this.req.params.id
  const body = this.req.body

  if (!id) {
    this.status(403)
    this.res.json({ message: 'Вы не выбрали событие!' })
  } else {

    if (!body) {
      this.res.json({ message: 'Вы ничего не изменили!' })
    }

    if (body.hasOwnProperty('approved') && body.hasOwnProperty('date')) {
      try {
        await EventModel.update({ _id: id }, {
          $set: {
            approved: body.approved,
            date: body.date
          }
        })
        this.res.json({ message: 'Вы успешно изменили дату и подтвердили!' })
      } catch (error) {
        this.res.status(503)
        this.res.json({ message: error.message })
      }
    }

    if (body.hasOwnProperty('approved')) {
      try {
        await EventModel.update({ _id: id }, { $set: { approved: body.approved }})
        this.res.json({ message: 'Вы успешно подтвердили!' })
      } catch (error) {
        this.res.status(503)
        this.res.json({ message: error.message })
      }
    }

    if (body.hasOwnProperty('date')) {
      try {
        await EventModel.update({ _id: id }, { $set: { date: body.date }})
        this.res.json({ message: 'Вы успешно изменили дату!' })
      } catch (error) {
        this.res.status(503)
        this.res.json({ message: error.message })
      }
    }

    this.res.json({ message: 'Вы ничего не изменили!' })
  }
}

EventController.prototype.deleteEvent = async function () {
  const id = this.req.params.id

  if (!id) {
    this.status(403)
    this.res.json({ message: 'Вы не выбрали событие!' })
  } else {
    try {
      await EventModel.findByIdAndRemove(id)
      this.res.json({ message: 'Вы успешно удалили событие!' })
    } catch (error) {
      this.res.status(503)
      this.res.json({ message: error.message })
    }
  }
}

module.exports = (req, res, next) => {

  const event = new EventController(req, res, next)

  switch (req.method) {
    case 'GET':
      event.getEvents()
      break
    case 'POST':
      event.postEvent()
      break
    case 'PUT':
      event.putEvent()
      break
    case 'DELETE':
      event.deleteEvent()
      break
    default:
      event.methodUndefined()
  }

}
