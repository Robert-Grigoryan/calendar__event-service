const server = {
  name: 'Event Service',
  port: 8080,
  host: 'localhost'
}

const database = {
  name: 'events',
  port: 63367,
  host: 'ds263367.mlab.com',
  user: 'root',
  pass: 'qwerty'
}

const routes = [
  {
    controller: require('./controllers/calendar'),
    requests: [
      {
        method: 'get',
        path: '/calendar'
      }
    ]
  },
  {
    controller: require('./controllers/event'),
    requests: [
      {
        method: 'get',
        path: '/event/:id'
      },
      {
        method: 'get',
        path: '/event'
      },
      {
        method: 'post',
        path: '/event'
      },
      {
        method: 'put',
        path: '/event/:id'
      },
      {
        method: 'del',
        path: '/event/:id'
      }
    ]
  }
]

module.exports = {
  server,
  database,
  routes
}
