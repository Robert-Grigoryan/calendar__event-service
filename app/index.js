const restify = require('restify')
const mongoose = require('mongoose')

module.exports = async (config) => {
  const user = config.database.user && config.database.pass ? `${config.database.user}:${config.database.pass}@` : ''
  const url = `${config.database.host}:${config.database.port}/${config.database.name}`
  const uri = `mongodb://${user}${url}`

  await mongoose.connect(uri)
    .then(() => {
      console.log('Database connected.')
    })

  const server = restify.createServer({
    name: config.server.name
  })

  server.use(restify.plugins.queryParser({
    mapParams: true
  }));

  server.use(restify.plugins.bodyParser({
    mapParams: true
  }));

  config.routes.forEach(route => {
    if (!route.controller) throw Error('Undefined controller!')

    route.requests.forEach(r => {
      if (!r.method) throw Error('Undefined method!')
      if (!r.path) throw Error('Undefined path!')

      server[r.method](r.path, route.controller)
    })
  })

  server.listen(config.server.port, () => {
    console.log('Server started.')
  })

  return server
}
