const mongoose = require('mongoose')
const moment = require('moment')

const scheme = mongoose.Schema({
  user: {
    type: String,
    required: true
  },
  comment: {
    type: String
  },
  date: {
    type: String,
    required: true
  },
  approved: {
    type: Boolean,
    default: false
  },
  createdAt: {
    type: String,
    default: moment().unix()
  }
})

const EventModel = mongoose.model('Event', scheme)

module.exports = EventModel
